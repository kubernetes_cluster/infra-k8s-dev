terraform {
  source = "${include.root.locals.source_url}//modules/main/networking/VPC?ref=${include.root.locals.source_version}"
}

include "root" {
  path   = find_in_parent_folders()
  expose = true
}

inputs = {
  vpc_cidr    = "10.0.0.0/16"
  public_cidr = ["10.0.10.0/24", "10.0.16.0/24"]
  privat_cidr = ["10.0.11.0/24", "10.0.17.0/24"]
  database_subnet = ["10.0..108.0/24", "10.0.109.0/24"]
  single_nat_gateway     = true
  one_nat_gateway_per_az = false
}
